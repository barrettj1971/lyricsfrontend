import React, { Component } from 'react';
import Navbar from './Components/Navbar';
import Results from './Components/Results';
import axios from 'axios';
import './App.css';

class App extends Component {
  
  state = {
    'artist': 'Search for a Band or Artist to begin',
    'titles': {},
    'page' : 1
  }
  
  nextPage = () => {
    this.setState((prevState) => {
      return {page:prevState.page+1};
    });
    this.pageSearch();
  }
  
  prevPage = () => {
    this.setState((prevState) => {
      return {page:prevState.page-1};
    });
    this.pageSearch();
  }
  
  pageSearch = () => {
    let searchTerm = document.getElementById('artistsearch').value;
    this.setState({'artist':searchTerm}); 
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/artist',{artist:searchTerm, page: this.state.page})
        .then(res => {
          this.setState({'titles':res.data.message.body.track_list});
        });
  }
  
  searchButtonClick = (e) => {
    e.preventDefault();
    let searchTerm = document.getElementById('artistsearch').value;
    this.setState({'artist':searchTerm, 'page': 1}); 
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/artist',{artist:searchTerm, page: this.state.page})
        .then(res => {
          this.setState({'titles':res.data.message.body.track_list});
        });
  }
  
  render() {
    return (
      <div>
        <Navbar searchButtonClick={this.searchButtonClick} />
        <Results artist={this.state.artist} titles={this.state.titles} page={this.state.page} nextPage={this.nextPage} prevPage={this.prevPage} />
      </div>
    );
  }
}

export default App;
