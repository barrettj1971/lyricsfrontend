import React, { Component } from 'react';

class Pagination extends Component {
  prev = () => {
    this.props.clearLyrics();
    this.props.prevPage();
  }
  nxt = () => {
    this.props.clearLyrics();
    this.props.nextPage();
  }
  render() {
    return (
          <div className="row">
            <div className="col-xs-4">
              {this.props.page > 1 && 
                <a className="btn btn-primary" onClick={() => this.prev()}>Previous</a>
              }
            </div>
            <div className="col-xs-4 text-center">Page {this.props.page}</div>
            <div className="col-xs-4 text-right">
                <a className="btn btn-primary" onClick={() => this.nxt()}>Next</a>
            </div>
          </div>
    );
  }
}

export default Pagination;
  