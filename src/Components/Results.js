import React, { Component } from 'react';
import ResultsList from './ResultsList';
import ResultsLyrics from './ResultsLyrics'; 
import Pagination from './Pagination'; 
import axios from 'axios';

class Results extends Component {
  state = {
    'lyrics': '',
    'track_name':'',
    'page':1
  }
  clearLyrics = () => {
    this.setState({'lyrics':'', 'track_name':''});
  }
  getLyrics = (lyrics_id,track_name) => {
    axios.post('https://quiet-savannah-60607.herokuapp.com/api/lyric',{lyrics_id:lyrics_id})
        .then(res => {
          if(res.data.message.body.lyrics) {
            this.setState({'lyrics':res.data.message.body.lyrics,'track_name':track_name});
          } else {
            this.setState({'lyrics':'', 'track_name':''});
          }
        });
  }
  
  render() {
    let listItems;
    if(this.props.titles.length) {
      listItems = this.props.titles.map(title => {
        return (
          <ResultsList track={title.track} key={title.track.lyrics_id} onClick={this.getLyrics}/>
        );
      });
    }
  
    return (
      <div className="container">
      
        <div className="row">
          <div className="col-md-6">
            <h2>{this.props.artist}</h2>
          </div>
        </div>
        
        {this.props.titles.length>1 && 
          <Pagination page={this.props.page} nextPage={this.props.nextPage} prevPage={this.props.prevPage} clearLyrics={this.clearLyrics}/>
        }
          <hr/>
        
          <div className="row">
          
          <div className="col-md-6">
            <ul className="list-group">
              {listItems}
            </ul>
          </div>
          { this.state.track_name.length > 0 && 
            <ResultsLyrics lyrics={this.state.lyrics} track_name={this.state.track_name} />
          }
          
        </div>
          
      </div>
    );
  }
}

export default Results;
