import React, { Component } from 'react';

class Navbar extends Component {
  
  render() {
    return (
      <nav className="navbar navbar-inverse">
      <div className="container">
        <div className="row">
          <div className="col-md-9">
            <a className="navbar-brand" href="">Lyrics Lookup</a>
          </div>

          <div className="col-md-3" style={{'paddingTop':'.5em'}}>
            <form onSubmit={this.props.searchButtonClick} >
              <div className="input-group">
                <input type="text" className="form-control" placeholder="Band/Artist" id="artistsearch"/>
                <span className="input-group-btn">
                  <button className="btn btn-default" type="submit" >Go!</button>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </nav>
    );
  }
}

export default Navbar;
