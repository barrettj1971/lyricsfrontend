import React, { Component } from 'react';

class ResultsLyrics extends Component {
  
  render() {
    return (
      <div className="col-md-6">
      	<h4>{this.props.track_name}</h4>
      	<div className="preWrap">{this.props.lyrics.lyrics_body}</div>
      </div>
    );
  }
}

export default ResultsLyrics;
