import React, { Component } from 'react';

class ResultsList extends Component {
  
  render() {
    return (
      <li className="list-group-item" onClick={() => this.props.onClick(this.props.track.track_id, this.props.track.track_name)}>
        {this.props.track.track_name} <small>by {this.props.track.artist_name} </small> <br/>
        <small> ( From the album {this.props.track.album_name} )</small>
        
      </li>
    );
  }
}

export default ResultsList;
  